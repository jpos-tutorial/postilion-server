package com.muhardin.endy.postilion.server;


import org.jpos.iso.*;
import org.jpos.iso.channel.PostChannel;
import org.jpos.iso.packager.PostPackager;
import org.jpos.util.LogSource;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;

public class EchoServer {
    public static void main(String[] args) throws Exception {
        System.out.println("Starting echo server ...");

        Logger logger = new Logger ();
        logger.addListener (new SimpleLogListener(System.out));
        ServerChannel channel = new PostChannel(new PostPackager());
        ((LogSource)channel).setLogger (logger, "channel");
        ISOServer server = new ISOServer (12345, channel, null);
        server.setLogger (logger, "postilion-server");

        server.addISORequestListener(new ISORequestListener() {
            @Override
            public boolean process(ISOSource isoSource, ISOMsg isoMsg) {
                try {
                    String strRequest = new String(isoMsg.pack());
                    System.out.println("Request : "+strRequest);
                    ISOMsg response = (ISOMsg) isoMsg.clone();
                    response.setMTI("0810");
                    response.set(39,"00");
                    String strResponse = new String(response.pack());
                    System.out.println("Response : "+strResponse);
                    isoSource.send(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });

        new Thread (server).start();
    }
}
